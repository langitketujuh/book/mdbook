# Pemasangan

## Memasang depedensi

Template ini menggunakan [mdBook](https://rust-lang.github.io/mdBook/) sebagai generator dokumentasinya.

* Memasang di langitketujuh/voidlinux:

```bash
sudo xbps-install -S mdBook mdbook-linkcheck
```

* Memasang di Archlinux atau manjaro

```bash
sudo pacman -Syy mdbook
```

* Memasang via cargo (hal ini mengkompil dari source code, tentu lumayan lama prosesnya)

```bash
export PATH="$PATH:~/.cargo/bin"
cargo install mdbook mdbook-linkcheck
```

## Kloning kode sumber

```bash
git clone --depth 1 https://gitlab.com/langitketujuh/book/mdbook.git
cd mdbook
```

## Menjalankan secara realtime

```bash
mdbook serve
```

## Membangun html

```bash
mdbook build
```

Hasil render berada di `book/html/`.

## Deploy ke netlify

Template ini sudah siap digunakan untuk deploy ke netlify. Anda tinggal merubah direktori build menjadi `book/html`.

## Demonstrasi website

Bisa anda kunjungi di https://mdbook.langitketujuh.id
